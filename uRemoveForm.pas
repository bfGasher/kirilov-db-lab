unit uRemoveForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  SQLite3, SQLiteTable3, uAddTrackForm;

type

  TRemoveRecArgs = packed record
    TableName: string;
    DB: TSQLiteDatabase;
    Fields: TStringList;
    constructor Create(const inDBLink: TSQLiteDatabase; const inTableName: string; const inFields: TStringList);
  end;

  TRemovePair = packed record
    Name: string;
    Value: string;
    constructor Create(const inName, inValue: string);
  end;

  TRemoveForm = class(TForm)
    OkButton: TButton;
    CloseButton: TButton;
    FieldLabel: TLabel;
    FieldValueEdit: TEdit;
    FieldComboBox: TComboBox;
    ValuesComboBox: TComboBox;
    procedure OkButtonClick(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FieldValueEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ValuesComboBoxChange(Sender: TObject);
  private
    fArgs: TRemoveRecArgs;
    function GetRemovePair: TRemovePair;
  public
    function Execute(const inArgs: TRemoveRecArgs): boolean;
    property Pair: TRemovePair read GetRemovePair;
  end;

var
  RemoveForm: TRemoveForm;

implementation

{$R *.dfm}

{ TRemoveForm }

procedure TRemoveForm.CloseButtonClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

function TRemoveForm.Execute(const inArgs: TRemoveRecArgs): boolean;
var
  s: string;
begin
  fArgs := inArgs;
  self.Caption := Format('�������� ������ �� ������ [%s].', [inArgs.TableName]);
  FieldComboBox.Items.Clear;
  for s in inArgs.Fields do
    FieldComboBox.Items.Add(s);
  FieldComboBox.ItemIndex := 0;
  result := ShowModal = mrOk;
end;

procedure TRemoveForm.FieldValueEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

function Prepare(const text: string): string;
var
  t: char;
  i1,i2:integer;
begin
  result := text;
  i1 := result.IndexOf('''');
  i2 := result.IndexOf('"');
  if (result.IndexOf('''') >= 0) or (result.INdexOf('"') >= 0) then
  begin
    // todo:
    t := result[Length(result)];
    result[Length(result)] := '%';
    result := result + t;
  end;
end;

var
  table: TSQLiteTable;
  value: string;
  name: string;
  query: string;
begin
  if Key = 13 then
  begin
    if fArgs.DB.TableExists(fArgs.TableName) then
    begin
      try
        name:= FieldComboBox.Items[FieldComboBox.ItemIndex];
        value := FieldValueEdit.Text;
        query := 'select * from ' + fArgs.TableName +
          ' where ' + name + ' LIKE ' + Prepare(value) + '; ';
        table := fArgs.DB.GetTable(query);
        if Assigned(table) then
        begin
          ValuesComboBox.Clear;
          while not table.Eof do
          begin
            ValuesComboBox.Items.Add(table.FieldByName[name]);
            table.Next;
          end;
          table.Free;
          if ValuesComboBox.Items.Count > 0 then
            ValuesComboBox.ItemIndex := 0;
        end;
      except
        on E: Exception do
          ShowMessage('Remove form sql error: ' + E.MEssage);
      end;
    end;
  end;
end;

procedure TRemoveForm.FormCreate(Sender: TObject);
begin
  // todo
  Self.OkButton.Caption := 'OK';
  self.CloseButton.Caption := '������';
  self.FieldLabel.Caption := '����';
  self.FieldValueEdit.Text := '';

  FieldValueEdit.Hint := '������ ����� � ����� Enter.'#13'���� ����� ���������� ��������� �������� �������� �� ��������.'#13'������ ����������� � ��������!!!';
  FieldValueEdit.ShowHint := true;

end;

function TRemoveForm.GetRemovePair: TRemovePair;
begin
  result := TRemovePair.Create(FieldComboBox.Items[FieldComboBox.ItemIndex], FieldValueEdit.Text);
end;

procedure TRemoveForm.OkButtonClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TRemoveForm.ValuesComboBoxChange(Sender: TObject);
var
  val: string;
  number: integer;
begin
  val := ValuesComboBox.Items[ValuesComboBox.ItemIndex];
  if not TryStrToInt(val, number) then
    val := '"' + val + '"';
  FieldValueEdit.Text := val;
end;

{ TRemoveRecArgs }

constructor TRemoveRecArgs.Create(const inDBLink: TSQLiteDatabase; const inTableName: string; const inFields: TStringList);
begin
  DB := inDBLink;
  TableName := inTableName;
  Fields := inFields;
end;

{ TRemovePair }

constructor TRemovePair.Create(const inName, inValue: string);
begin
  Name := inName;
  Value := inValue;
end;

end.
