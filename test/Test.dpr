program Test;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.Generics.Collections,
  uDB, uTrack;

function Loading(): boolean;
var
  lTracks: TList<TTrackRec>;
begin
  result := true;
  lTracks := LoadFromFile('data.txt');
  if not Assigned(lTracks) or (lTracks.Count <= 0) then
    result := false;
  lTracks.Free;
end;

function DataBaseShowTable: boolean;
var
  DataBase: TDataBase;
  table: TTable;
begin
  result := true;
  DataBase := TDataBase.Create('test');
  DataBase.Connect;

  Table := DataBase.LoadTable('memes');
  if Assigned(Table) then
  begin
    // todo:
    Table.Free();
  end;

  DataBase.Disconnect;
end;

function GetResult(inExpression: boolean): string;
begin
  case inExpression of
    true: result := 'ok';
    false: result := 'bad';
  end;
end;

procedure TestCase(const inName: string; inResult: boolean);
begin
  WriteLn(inName, '  ', GetResult(inResult));
end;

begin
  TestCase('Loading', Loading());
  TestCase('Database', DataBaseShowTable());
  ReadLn;
end.
