unit uProgressForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls;

type

  TProgressForm = class(TForm)
    ProgressBar: TProgressBar;
  strict private
    fStart: integer;
    fMinCount: integer;
    fCount: integer;
    fDelta: integer;
    function CanShow(): boolean;
    function IsValidPosition(const Value: integer): boolean;
  public
    constructor Create(const Caption: string;
                       const MinCount: integer;
                       const Start: integer;
                       const Count: integer);
    function Start(): boolean;
    function Update(const Position: integer): boolean;
    function Next(): boolean;

    property Count: integer read fCount write fCount;
    property Delta: integer read fDelta write fDelta;
  end;

implementation

{$R *.dfm}

constructor TProgressForm.Create(const Caption: string;
                                 const MinCount: integer;
                                 const Start: integer;
                                 const Count: integer);
begin
  inherited Create(nil);
  self.fDelta    := 1;
  self.fMinCount := MinCount;
  self.fStart    := Start;
  self.fCount    := Count;
  self.Caption   := Caption;
end;

function TProgressForm.IsValidPosition(const Value: integer): boolean;
begin
  result := (Value >= fStart) and (Value <= fCount);
end;

function TProgressForm.Next: boolean;
begin
  result := true;
  if CanShow then
  begin
    self.ProgressBar.Position := self.ProgressBar.Position + fDelta;
    ProgressBar.Update();
  end;
  Application.ProcessMessages(); // todo;
end;

function TProgressForm.Start: boolean;
begin
  ModalResult := mrOk;
  result := true;
  ProgressBar.Min := fStart;
  ProgressBar.Max := fCount;
  if CanShow then
  begin
    self.ProgressBar.Position := fStart;
    Show;
  end;
end;

function TProgressForm.Update(const Position: integer): boolean;
begin
  result := true;
  if CanShow then
  begin
    if IsValidPosition(Position) then
      self.ProgressBar.Position := Position
    else
      result := false;
  end;
end;

function TProgressForm.CanShow: boolean;
begin
  result := fMinCount < fCount;
end;

end.

