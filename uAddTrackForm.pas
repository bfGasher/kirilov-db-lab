unit uAddTrackForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,
  System.Variants, System.Classes, Vcl.Graphics,  System.Generics.Collections,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Vcl.Grids, Vcl.ValEdit, Vcl.CategoryButtons;

type

  TAddRecArgs = packed record
    TableName: string;
    Fields: TStringList;
    constructor Create(const inTableName: string; const inFields: TStringList);
    procedure Dispose;
  end;

  TAddTrackForm = class(TForm)
    OkButton: TButton;
    CancelButton: TButton;
    FieldsValuesListEditor: TValueListEditor;
    ClearButton: TButton;
    procedure FormCreate(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure ClearButtonClick(Sender: TObject);
  private
    { Private declarations }
    fCurrentTableName: string;
    function GetElemLines(): TArray<String>;
  public
    { Public declarations }
    function Execute(const inArgs: TAddRecArgs): Boolean;
    property ElemLines: TArray<string> read GetElemLines;
  end;

var
  AddTrackForm: TAddTrackForm;

implementation

{$R *.dfm}

procedure TAddTrackForm.CancelButtonClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TAddTrackForm.ClearButtonClick(Sender: TObject);
var
  i: integer;
begin
  for i := 1 to FieldsValuesListEditor.ColCount do
    FieldsValuesListEditor.Values[FieldsValuesListEditor.Keys[i]] := '';
end;

function TAddTrackForm.Execute(const inArgs: TAddRecArgs): Boolean;
var
  s: string;
begin
  fCurrentTableName := inArgs.TableName;

  FieldsValuesListEditor.Strings.Clear;
  for s in inArgs.Fields do
    self.FieldsValuesListEditor.InsertRow(s, ' ', true);

  if fCurrentTableName = '' then
  begin
    fCurrentTableName := '<unknown>';
    ShowMessage('Table name is incorrect. Try again after selecting correct table.');
  end;
  self.Caption := Format('�������� ������ � ������� [%s]', [fCurrentTableName]);
  result := ShowModal = mrOk;
end;

procedure TAddTrackForm.FormCreate(Sender: TObject);
begin
  self.Caption := '';
  ClearButton.Caption := '��������';
  CancelButton.Caption := '������';
  OkButton.Caption     := 'OK';
end;

function TAddTrackForm.GetElemLines: TArray<String>;
begin
  result := FieldsValuesListEditor.Strings.ToStringArray;
end;

procedure TAddTrackForm.OkButtonClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

{ TAddRecArgs }

constructor TAddRecArgs.Create(const inTableName: string;
  const inFields: TStringList);
begin
  self.TableName := inTableName;
  self.Fields := inFields;
  if not Assigned(Fields) then
    Fields := TStringList.Create();
end;

procedure TAddRecArgs.Dispose;
begin
  if Assigned(Fields) then
    FreeAndNil(Fields);
  SetLength(TableName, 0);
end;

end.
