program DataBaseLab;

uses
  Forms,
  uMainForm in 'uMainForm.pas' {MainForm},
  uAddTrackForm in 'uAddTrackForm.pas' {AddTrackForm},
  uLogger in 'uLogger.pas',
  SQLite3 in 'Sqlite\SQLite3.pas',
  sqlite3udf in 'Sqlite\sqlite3udf.pas',
  SQLiteTable3 in 'Sqlite\SQLiteTable3.pas',
  uRemoveForm in 'uRemoveForm.pas' {RemoveForm},
  uQuerys in 'uQuerys.pas',
  uProgressForm in 'uProgressForm.pas' {ProgressForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAddTrackForm, AddTrackForm);
  Application.CreateForm(TRemoveForm, RemoveForm);
  Application.Run;
end.
