object MainForm: TMainForm
  Left = 165
  Top = 62
  Caption = 'MainForm'
  ClientHeight = 631
  ClientWidth = 884
  Color = clBtnFace
  Constraints.MinHeight = 690
  Constraints.MinWidth = 900
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    884
    631)
  PixelsPerInch = 96
  TextHeight = 13
  object GridPanel: TPanel
    Left = 8
    Top = 87
    Width = 868
    Height = 234
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    DesignSize = (
      868
      234)
    object MainLabel: TLabel
      Left = 9
      Top = 9
      Width = 47
      Height = 13
      Caption = 'MainLabel'
    end
    object MainGrid: TStringGrid
      Left = 9
      Top = 28
      Width = 848
      Height = 182
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
  end
  object ControlPanel: TPanel
    Left = 8
    Top = 8
    Width = 561
    Height = 73
    TabOrder = 1
    object SortSettingsLabel: TLabel
      Left = 9
      Top = 40
      Width = 84
      Height = 13
      Caption = 'SortSettingsLabel'
    end
    object FieldLabel: TLabel
      Left = 9
      Top = 13
      Width = 47
      Height = 13
      Caption = 'FieldLabel'
    end
    object DisplayButton: TButton
      Left = 227
      Top = 37
      Width = 75
      Height = 25
      Caption = 'DisplayButton'
      TabOrder = 0
      OnClick = DisplayButtonClick
    end
    object AddButton: TButton
      Left = 308
      Top = 37
      Width = 75
      Height = 25
      Caption = 'AddButton'
      TabOrder = 1
      OnClick = AddButtonClick
    end
    object ExitButton: TButton
      Left = 470
      Top = 10
      Width = 75
      Height = 52
      Caption = 'ExitButton'
      TabOrder = 2
      OnClick = ExitButtonClick
    end
    object RemoveButton: TButton
      Left = 389
      Top = 37
      Width = 75
      Height = 25
      Caption = 'RemoveButton'
      TabOrder = 3
      OnClick = RemoveButtonClick
    end
    object SortOrderComboBox: TComboBox
      Left = 98
      Top = 37
      Width = 123
      Height = 21
      TabOrder = 4
      Text = 'SortOrderComboBox'
      OnChange = SortOrderComboBoxChange
    end
    object YearLimitComboBox: TComboBox
      Left = 227
      Top = 10
      Width = 122
      Height = 21
      TabOrder = 5
      Text = 'SortOrderComboBox'
      OnChange = YearLimitComboBoxChange
    end
    object YearLimitEdit: TEdit
      Left = 355
      Top = 10
      Width = 109
      Height = 21
      TabOrder = 6
    end
    object FieldComboBox: TComboBox
      Left = 98
      Top = 10
      Width = 123
      Height = 21
      TabOrder = 7
      Text = 'FieldComboBox'
    end
  end
  object SubPanel: TPanel
    Left = 8
    Top = 327
    Width = 868
    Height = 296
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'SubPanel'
    TabOrder = 2
    DesignSize = (
      868
      296)
    object SortedLabel: TLabel
      Left = 9
      Top = 13
      Width = 57
      Height = 13
      Caption = 'SortedLabel'
    end
    object SubStringGrid: TStringGrid
      Left = 9
      Top = 32
      Width = 848
      Height = 249
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
    end
  end
  object BdSelectPanel: TPanel
    Left = 584
    Top = 8
    Width = 292
    Height = 73
    TabOrder = 3
    object BDSelectLabel: TLabel
      Left = 8
      Top = 13
      Width = 67
      Height = 13
      Caption = 'BDSelectLabel'
    end
    object BDSelectComboBox: TComboBox
      Left = 8
      Top = 32
      Width = 273
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = BDSelectComboBoxChange
    end
  end
  object MainMenu: TMainMenu
    Left = 16
    Top = 16
    object MainMenuItem: TMenuItem
      Caption = 'MainMenuItem'
      object ClearMenuItem: TMenuItem
        Caption = 'ClearMenuItem'
        OnClick = ClearMenuItemClick
      end
      object LoadFromFileMenuItem: TMenuItem
        Caption = 'LoadFromFileMenuItem'
        Enabled = False
        OnClick = LoadFromFileMenuItemClick
      end
      object LoadDefaultMenuItem: TMenuItem
        Caption = 'LoadDefaultMenuItem'
        OnClick = LoadDefaultMenuItemClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object ExitMenuItem: TMenuItem
        Caption = 'ExitMenuItem'
        OnClick = ExitMenuItemClick
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 48
    Top = 32
  end
end
