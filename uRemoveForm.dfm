object RemoveForm: TRemoveForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'RemoveForm'
  ClientHeight = 70
  ClientWidth = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object FieldLabel: TLabel
    Left = 8
    Top = 13
    Width = 35
    Height = 13
    Caption = 'IdLabel'
  end
  object OkButton: TButton
    Left = 394
    Top = 8
    Width = 75
    Height = 25
    Caption = 'OkButton'
    TabOrder = 0
    OnClick = OkButtonClick
  end
  object CloseButton: TButton
    Left = 475
    Top = 8
    Width = 75
    Height = 25
    Caption = 'CloseButton'
    TabOrder = 1
    OnClick = CloseButtonClick
  end
  object FieldValueEdit: TEdit
    Left = 183
    Top = 10
    Width = 205
    Height = 21
    TabOrder = 2
    OnKeyDown = FieldValueEditKeyDown
  end
  object FieldComboBox: TComboBox
    Left = 49
    Top = 10
    Width = 128
    Height = 21
    Style = csDropDownList
    TabOrder = 3
  end
  object ValuesComboBox: TComboBox
    Left = 183
    Top = 37
    Width = 205
    Height = 21
    TabOrder = 4
    OnChange = ValuesComboBoxChange
  end
end
