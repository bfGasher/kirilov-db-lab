unit uMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages,
  System.SysUtils, System.Variants, System.Classes,
  System.Generics.Collections,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Data.DB, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, uLogger,
  SQLiteTable3,  Vcl.Menus;

type

  TSortOrder = (soAsc, soDesc);
  TLimitOrder = (loNone,loAfter, loBefore);

  TMainForm = class(TForm)
    GridPanel: TPanel;
    ControlPanel: TPanel;
    SortSettingsLabel: TLabel;
    DisplayButton: TButton;
    AddButton: TButton;
    ExitButton: TButton;
    SubPanel: TPanel;
    RemoveButton: TButton;
    SortOrderComboBox: TComboBox;
    YearLimitComboBox: TComboBox;
    YearLimitEdit: TEdit;
    MainLabel: TLabel;
    SortedLabel: TLabel;
    MainGrid: TStringGrid;
    SubStringGrid: TStringGrid;
    BdSelectPanel: TPanel;
    BDSelectLabel: TLabel;
    BDSelectComboBox: TComboBox;
    FieldComboBox: TComboBox;
    FieldLabel: TLabel;
    MainMenu: TMainMenu;
    MainMenuItem: TMenuItem;
    N2: TMenuItem;
    ExitMenuItem: TMenuItem;
    ClearMenuItem: TMenuItem;
    LoadDefaultMenuItem: TMenuItem;
    LoadFromFileMenuItem: TMenuItem;
    OpenDialog: TOpenDialog;
    //
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //
    procedure ExitButtonClick(Sender: TObject);
    procedure AddButtonClick(Sender: TObject);
    procedure SortOrderComboBoxChange(Sender: TObject);
    procedure YearLimitComboBoxChange(Sender: TObject);
    procedure DisplayButtonClick(Sender: TObject);
    procedure RemoveButtonClick(Sender: TObject);
    procedure BDSelectComboBoxChange(Sender: TObject);
    procedure ExitMenuItemClick(Sender: TObject);
    procedure ClearMenuItemClick(Sender: TObject);
    procedure LoadDefaultMenuItemClick(Sender: TObject);
    procedure LoadFromFileMenuItemClick(Sender: TObject);
 strict private
    fSortOrder: TSortOrder;
    fLimitOrder: TLimitOrder;
    fbase: TSQLiteDatabase;
    fFields: TStringList;
    fCurrentTableName: string;
    fTableNames: TStringList;
    //
    procedure CreateDataBase();
    procedure LoadTableNames();
    procedure LoadTableFields(const inTablename: string);
    //
    procedure FillCaptions;
    procedure DropAllTables();
    procedure ShowAllTables();
    procedure ShowTable();
    procedure ShowSortedTable();
  end;

const
  ApplicationVersion = '1.1';

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses uRemoveForm, uAddTrackForm, uLoadFromFileForm, uProgressForm;

function LocalGetRoot(): string;
begin
  result := ExtractFilePath(ParamStr(0));
end;

procedure TMainForm.RemoveButtonClick(Sender: TObject);
var
  pair: TRemovePair;
begin
  if fCurrentTableName <> '' then
  begin
    if RemoveForm.Execute(TRemoveRecArgs.Create(fBase, fCurrentTableName, fFields)) then
    begin
      pair := RemoveForm.Pair;
      if (pair.Name <> '') and (pair.Value <> '') then
      begin
        // todo: implement wise query for data removing
        try
          fbase.ExecSQL('delete from ' + fCurrentTableName + ' where ' + pair.Name + ' = ' + pair.Value);
        except
          on E: Exception do
          begin
            ShowMessage('Sql delete error: ' + E.Message);
          end;
        end;
        Log.Log(Format('Record with field %s and value %s was removed from table %s.', [Pair.Name, Pair.Value, fCurrentTableName]));
      end
      else
        ShowMessage('Remove data invalid. Input not empty data please.');
    end
    else
      Log.Log('Remove not finished.');
    ShowAllTables;
  end
  else
    ShowMessage('Incorrect table name.');
end;

procedure TMainForm.AddButtonClick(Sender: TObject);

  function MakeValuesStr(const aLines: TArray<string>): string;
  var
    s: string;
  begin
    result := '';
    result := result + '(';
    for s in aLines do
    begin
      result := result + s.Remove(0, s.IndexOf('=') + 1) + ',';
    end;
    result := result.Remove(result.LastIndexOf(','));
    result := result  + ')';
  end;

  function Preprocess(const inStr: string): string;
  begin
    result := inStr.Remove(inStr.IndexOf('='));
  end;

var
  values: TArray<string>;
begin
  if fCurrentTableName <> '' then
  begin
    if AddTrackForm.Execute(TAddRecArgs.Create(fCurrentTableName, fFields)) then
    begin
      values := AddTrackForm.ElemLines;
      try
        fBase.ExecSQL('insert into ' + fCurrentTableName + ' values ' + MakeValuesStr(values));
      except
        on E: Exception do
          ShowMessage('Sql insert error: ' + E.Message);
      end;
      MainGrid.RowCount := MainGrid.RowCount + 1;
      SubStringGrid.RowCount := SubStringGrid.RowCount + 1;
    end;
    ShowAllTables;
  end
  else
    ShowMessage('Incorrect table name');
end;

procedure TMainForm.BDSelectComboBoxChange(Sender: TObject);
begin
  fCurrentTableName := BDSelectComboBox.Items[BDSelectComboBox.ItemIndex];
  if fCurrentTableName = '' then
    ShowMessage('table name is empty. this is baad!')
  else
  begin
    LoadTableFields(fCurrentTableName);
    ShowAllTables;
  end;
end;

procedure TMainForm.ClearMenuItemClick(Sender: TObject);
begin
  DropAllTables;
  LoadTableNames;
  LoadTableFields(fCurrentTableName);
  ShowAllTables;
end;

procedure TMainForm.CreateDataBase;
begin
  fbase := TSQLiteDatabase.Create(LocalGetRoot() + 'test.sdb');
  Log.Log('database created');
end;

procedure TMainForm.DisplayButtonClick(Sender: TObject);
begin
  ShowTable;
  ShowSortedTable;
  Log.Log('refreshing table view on main window');
end;

procedure TMainForm.DropAllTables;
var
  table: string;
begin
   try
    for table in fTableNames do
    begin
      if fBase.TableExists(table) then
        fBase.ExecSql('drop table ' + table);
    end;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  fCurrentTableName := '';
  fTableNames.Clear;
  fFields.Clear;
  FieldComboBox.Clear;
  BDSelectComboBox.Clear;
end;

procedure TMainForm.ExitButtonClick(Sender: TObject);
begin
  Close;
  Log.Log('CLose button pressed');
end;

procedure TMainForm.ExitMenuItemClick(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.FillCaptions;
begin
  Log.Log('Settup captions');

  self.MainMenuItem.Caption         := '����';
  self.ExitMenuItem.Caption         := '�����';
  self.ClearMenuItem.Caption        := '��������';
  self.LoadFromFileMenuItem.Caption := '���������...';
  self.LoadDefaultMenuItem.Caption  := '���� ��-���������';

  self.Caption               := '�������� ������ ' + ApplicationVersion;
  self.ExitButton.Caption    := '�����';
  self.DisplayButton.Caption := '��������';
  self.AddButton.Caption     := '��������';
  self.RemoveButton.Caption  := '�������';

  self.BDSelectLabel.Caption := '�������';
  self.BdSelectPanel.Caption := '';
  self.BDSelectComboBox.Text := '';

  self.FieldLabel.Caption        := '����';
  self.SortSettingsLabel.Caption := '����������';
  self.MainLabel.Caption         := '������';
  self.SortedLabel.Caption       := '������ � ��������';

  self.SortOrderComboBox.Items.Clear;
  self.SortOrderComboBox.Items.Add('�� �����������');
  self.SortOrderComboBox.Items.Add('�� ��������');
  self.SortOrderComboBox.Text := self.SortOrderComboBox.Items[0];

  self.YearLimitEdit.Text := '';
  self.YearLimitEdit.NumbersOnly := false;

  self.YearLimitComboBox.Items.Clear;
  self.YearLimitComboBox.Items.Add('��� ����');
  self.YearLimitComboBox.Items.Add('��');
  self.YearLimitComboBox.Items.Add('�����');
  self.YearLimitComboBox.Text := self.YearLimitComboBox.Items[0];

  self.SubStringGrid.ColCount  := 7;
  self.SubStringGrid.FixedCols := 0;
  self.SubStringGrid.RowCount  := 10;

  self.MainGrid.ColCount  := 7;
  self.MainGrid.FixedCols := 0;
  self.MainGrid.RowCount  := 10;

  self.FieldComboBox.Clear;

  Log.Log('Caption setup finished');
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Log.Log('Creating main form');
  fFields := TStringList.Create();
  fTableNames := TStringList.Create();

  FillCaptions;
  CreateDataBase;

  fSOrtOrder  := soAsc;
  fLimitOrder := loNone;

  LoadTableNames;
  LoadTableFields(fCurrentTableName);

  ShowTable;
  ShowSortedTable;
  Log.Log('Main form created');
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  Log.Log('dispose memory resources');
  if Assigned(fBase) then
    fBase.Free;
  if Assigned(fTableNames) then
    fTableNames.Free;
  if Assigned(fFields) then
    fFields.Free;
end;

procedure TMainForm.LoadDefaultMenuItemClick(Sender: TObject);
var
  text: TStringList;
  t: string;
  Progress: TProgressForm;
begin
  text := TStringList.Create;
  Progress := TProgressForm.Create('Loading...', 10, 0, text.Count);
  Progress.Parent := self;
  try
    text.LoadFromFile('queryes.txt');
    Progress.Count := text.Count;
    Progress.Start();
    try
      for t in text do
      begin
        if t <> '' then
          fBase.ExecSql(t);
        Progress.Next();
      end;
      Progress.Close();
    except
      on E: Exception do
      begin
        ShowMessage('Sql error: ' + E.Message);
      end;
    end;
  finally
    Progress.Free();
    text.Free();
  end;
  LoadTableNames;
  LoadTableFields(fCurrentTableName);
  ShowAllTables;
end;

procedure TMainForm.LoadFromFileMenuItemClick(Sender: TObject);
var
  text: TStringList;
begin
  OpenDialog.InitialDir := LocalGetRoot;
  if OpenDialog.Execute then
  begin
    text := TStringList.Create;
    try
      text.LoadFromFile(OpenDialog.FileName);
    finally
      text.Free;
    end;
  end;
  ShowAllTables;
end;

procedure TMainForm.LoadTableFields(const inTablename: string);

  function GetBrackets(const inText:string): string;
  var
    start: integer;
  begin
    result := inText;
    start := result.IndexOf('(');
    result := result.Remove(0, start + 1);
    result := result.Remove(result.Length - 1);
  end;

  function TrimWhitespaces(const inText: string): string;
  begin
    result := inText;
    result := result.Trim([' ']);
  end;

var
  table: TSQLiteTable;
  Line: string;
  s, str: string;
  strings: TArray<string>;
begin
  FieldComboBox.Enabled := false;
  FieldComboBox.items.CLear;
  fFields.Clear;
  if inTablename <> '' then
  begin
    table := fBase.GetTable('select name, sql from sqlite_master where name = "'+ inTablename + '" and type = "table";');
    try
      if not table.EOF then
      begin
        fFields.Clear;
        Line := table.FieldByName['sql'];
        Line := GetBrackets(Line);
        Line := Line.Trim;
        //
        strings := Line.Split([',']);
        Log.Log('fields:');
        for str in strings do
        begin
          s := str;
          s := s.Trim();
          if s.IndexOf('foreign') < 0 then
          begin
            s := s.Remove(s.IndexOf(' '));
            fFields.Add(s);
          end;
          Log.Log(s);
        end;
        SetLength(strings, 0);
      end
      else
        ShowMessage('No tables found on database.');
    finally
      table.Free;
    end;
    if fFields.Count > 0 then
    begin
      for s in fFields do
        FieldComboBox.Items.Add(s);
      FieldComboBox.ItemIndex := 0;
    end;
    FieldComboBox.Enabled := true;
  end;

end;

procedure TMainForm.LoadTableNames;
var
  Table: TSQLiteTable;
  Line: string;
begin
  fCurrentTableName := ''; // debug:
  BDSelectComboBox.Items.Clear;
  fTableNames.Clear;
  Table := fBase.GetTable('select name from sqlite_master where type = "table";');
  try
    Log.Log('begining fetching table names from database');
    while not Table.Eof do
    begin
      Line :=Table.FieldByName['name'];
      BDSelectComboBox.Items.Add(Line);
      fTableNames.Add(Line);
      Log.Log(Line);
      Table.Next;
    end;
    Log.Log('tables obtained');
  finally
    Table.Free;
  end;
  if BDSelectComboBox.Items.Count > 0 then
  begin
    BDSelectComboBox.ItemIndex := 0;
    fCurrentTableName := BDSelectComboBox.Items[BDSelectComboBox.ItemIndex];
  end;
end;

procedure TMainForm.ShowAllTables;
begin
  ShowTable;
  ShowSortedTable;
end;

procedure TMainForm.ShowSortedTable;
  // ����� ������� ���������� ???
  function GetAscDescOrder: string;
  begin
    result := '';
    case fSortOrder of
      soAsc : result := 'ASC';
      soDesc : result := 'DESC';
    end;
  end;

  // �� ������ ���� ���������� ����������
  function GetSortFieldName(): string;
  begin
    result := 'order by ' + FieldComboBox.Items[FieldComboBox.ItemIndex];
    if result <> '' then
      result := result  + ' ' + GetAscDescOrder();
  end;

  function GetLimits: string;
  var
    sign: string;
  begin
    // todo:
    result := '';
    if Length(YearLimitEdit.Text) > 0 then
    begin
      if string.toInteger(YearLimitEdit.Text) > 0 then
      begin
        case self.fLimitOrder of
          loNone: Sign := '';
          loAfter: Sign:= '>=';
          loBefore: Sign := '<=';
        end;
        if Sign <> '' then
          result := 'where ' + FieldComboBox.Items[FieldComboBox.ItemIndex]+ ' ' + Sign + ' ' + YearLimitEdit.Text;
      end;
    end;
  end;

  function MakeSqlString: string;
  begin
    result := 'select * from ' + fCurrentTableName + ' ' + GetLimits + ' ' + GetSortFieldName()
  end;

var
  Table: TSQLiteTable;
  index: integer;
  i,j:integer;
  s: string;

begin
  SubStringGrid.Rows[0].Clear;
  for index := 1 to SubStringGrid.RowCount - 1 do
    SubStringGrid.Rows[index].Clear;

  SubStringGrid.RowCount := fFields.Count;
  for s in fFields do
    SubStringGrid.Rows[0].Add(s);

  if fBase.TableExists(fCurrentTableName) then
  begin
    Table := fBase.GetTable(MakeSqlString());
    if Assigned(Table) then
    begin

      index := 0;
      SubStringGrid.RowCount := Table.Count;
      while not Table.Eof do
      begin
        i := 0;
        j := index + 1;
        for s in fFields do
        begin
          SubStringGrid.Cells[i, j] := Table.FieldByName[s];
          Inc(i);
        end;
        Inc(index);
        SubStringGrid.RowCount := SubStringGrid.RowCount + 1;
        Table.Next;
      end;
      Table.Free;
    end;
  end;

end;

procedure TMainForm.ShowTable();
var
  Table: TSQLiteTable;
  index: integer;
  i, j: integer;
  s: string;
begin

  for index := 1 to MainGrid.RowCount - 1 do
    MainGrid.Rows[index].Clear;

  MainGrid.RowCount := fFields.Count;
  MainGrid.Rows[0].Clear;

  for s in fFields do
    MainGrid.Rows[0].Add(s);
  if fBase.TableExists(fCurrentTableName) then
  begin
    Table := fBase.GetTable('select * from ' + fCurrentTableName);
    if Assigned(Table) then
    begin
      for index := 1 to MainGrid.RowCount - 1 do
        MainGrid.Rows[index].Clear;
      MainGrid.RowCount := Table.Count;
      index := 0;
      while not Table.Eof do
      begin
        i := 0;
        j := index + 1;
        for s in fFields do
        begin
          MainGrid.Cells[i, j] := Table.FieldByName[s];
          Inc(i);
        end;
        Inc(index);
        MainGrid.RowCount := MainGrid.RowCount + 1;
        Table.Next;
      end;
      Table.Free;
    end;
  end;
end;

procedure TMainForm.SortOrderComboBoxChange(Sender: TObject);
begin
  if SortOrderComboBox.Items[SortOrderCOmboBox.ItemIndex] = '�� �����������' then
    fSOrtOrder := soAsc;
  if SortOrderComboBox.Items[SortOrderCOmboBox.ItemIndex] = '�� ��������' then
    fSortOrder := soDesc;
end;

procedure TMainForm.YearLimitComboBoxChange(Sender: TObject);
begin
  if YearLImitComboBox.Items[YearLimitComboBox.ItemIndex] = '��� ����' then
    fLimitOrder := loNone;
  if YearLimitComboBox.Items[YearLimitComboBox.ItemIndex] = '��' then
    fLimitOrder := loBefore;
  if YearLimitComboBox.Items[YearLimitComboBox.ItemIndex] = '�����' then
    fLimitOrder := loAfter;
end;

end.
