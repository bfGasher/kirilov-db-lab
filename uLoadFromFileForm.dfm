object LoadFromFileForm: TLoadFromFileForm
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'LoadFromFileForm'
  ClientHeight = 80
  ClientWidth = 405
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object FilePathLabel: TLabel
    Left = 8
    Top = 8
    Width = 63
    Height = 13
    Caption = 'FilePathLabel'
  end
  object OkButton: TButton
    Left = 8
    Top = 35
    Width = 75
    Height = 25
    Caption = 'OkButton'
    TabOrder = 0
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 89
    Top = 35
    Width = 75
    Height = 25
    Caption = 'CancelButton'
    TabOrder = 1
    OnClick = CancelButtonClick
  end
  object FilePathEdit: TEdit
    Left = 51
    Top = 5
    Width = 307
    Height = 21
    TabOrder = 2
    Text = 'FilePathEdit'
  end
  object FindFileButton: TButton
    Left = 364
    Top = 5
    Width = 29
    Height = 21
    Caption = '...'
    TabOrder = 3
    OnClick = FindFileButtonClick
  end
  object OpenDialog: TOpenDialog
    Left = 312
    Top = 32
  end
end
