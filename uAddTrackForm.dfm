object AddTrackForm: TAddTrackForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'AddTrackForm'
  ClientHeight = 263
  ClientWidth = 551
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object OkButton: TButton
    Left = 8
    Top = 215
    Width = 75
    Height = 25
    Caption = 'OkButton'
    TabOrder = 0
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 89
    Top = 215
    Width = 75
    Height = 25
    Caption = 'CancelButton'
    TabOrder = 1
    OnClick = CancelButtonClick
  end
  object FieldsValuesListEditor: TValueListEditor
    Left = 8
    Top = 8
    Width = 525
    Height = 201
    KeyOptions = [keyDelete, keyUnique]
    TabOrder = 2
    ColWidths = (
      150
      369)
  end
  object ClearButton: TButton
    Left = 170
    Top = 215
    Width = 75
    Height = 25
    Caption = 'ClearButton'
    TabOrder = 3
    OnClick = ClearButtonClick
  end
end
