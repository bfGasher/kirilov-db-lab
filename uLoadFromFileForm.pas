unit uLoadFromFileForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TLoadFromFileForm = class(TForm)
    FilePathLabel: TLabel;
    OkButton: TButton;
    CancelButton: TButton;
    FilePathEdit: TEdit;
    FindFileButton: TButton;
    OpenDialog: TOpenDialog;
    procedure CancelButtonClick(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure FindFileButtonClick(Sender: TObject);
  public
    function Execute(): boolean;
  end;

var
  LoadFromFileForm: TLoadFromFileForm;

implementation

{$R *.dfm}

procedure TLoadFromFileForm.CancelButtonClick(Sender: TObject);
begin
  Modalresult := mrCancel;
end;

function TLoadFromFileForm.Execute: boolean;
begin
  result := ShowModal = mrOk;
end;

procedure TLoadFromFileForm.FindFileButtonClick(Sender: TObject);
begin
  OpenDialog.Filter := '(*.csv)';
  if self.OpenDialog.Execute then
  begin
    // todo
    self.FilePathEdit.Text := OpenDialog.FileName;
  end;
end;

procedure TLoadFromFileForm.OkButtonClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
