unit uLogger;

interface

uses
  System.SysUtils;

type

  TLogger = class
  public
    procedure FormatLog(const inFormat: string); virtual; abstract;
    procedure Log(const inText: string); virtual; abstract;
  end;

  TFileLogger = class(TLogger)
  strict private
    fFileName: string;
    function GetDateString(inDate: TDateTime): string;
  public
    constructor Create(const inFileName: string);
    procedure FormatLog(const inFormat: string); override;
    procedure Log(const inText: string); override;
  end;

  TConsoleLogger = class (TLogger)
  public
    procedure FormatLog(const inFormat: string); override;
    procedure Log(const inText: string); override;
  end;

var
  Log: TLogger;

implementation

{ TConsoleLogger }

procedure TConsoleLogger.FormatLog(const inFormat: string);
begin
  // todo: implement via Format function
  WriteLn(inFormat);
end;

procedure TConsoleLogger.Log(const inText: string);
begin
  WriteLn(inText);
end;

{ TFileLogger }

constructor TFileLogger.Create(const inFileName: string);

  procedure CreateFile;
  var
    handle: TextFile;
  begin
    AssignFile(handle, fFileName);
    ReWrite(handle);
    Write(handle, '');
    CloseFile(handle);
  end;

begin
  fFileName := inFileName;
  if fFileName = '' then
    fFileName := Format('log%s.txt', [GetDateString(Now)]);;
  CreateFile;
end;

procedure TFileLogger.FormatLog(const inFormat: string);
begin
  // todo: implement it with formated string input
  Log(inFormat);
end;

function TFileLogger.GetDateString(inDate: TDateTime): string;
begin
  result := DateToStr(inDate);
end;

procedure TFileLogger.Log(const inText: string);
var
  handle: TextFile;
begin
  AssignFile(Handle, fFileName);
  Append(Handle);
  WriteLn(Handle,'[', GetDateString(Now), '] ', inText);
  CloseFile(Handle);
end;

initialization
  Log := TFileLogger.Create('log.txt');
finalization
  Log.Free;
end.
